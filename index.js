const express = require(`express`);
const http = require(`http`)
const mongoose = require('mongoose');
const dotenv = require(`dotenv`);
dotenv.config();
const app = express();
const PORT = 3007;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection
db.on("error", console.error.bind(console, `connection error:`))
db.once("open", () => console.log(`Connected to database`))


const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]     
        },
        status: {
            type: String,
            default: "pending"
        }
    }
)

const userModel = mongoose.model(`User`, userSchema)
app.post(`/signup`, (req, res) => {
    userModel.findOne({name: req.body.name}).then((result, err) =>{
        console.log(result)

        if(result != null && result.name == req.body.name){
            return res.send(`Duplicate task Found`)
        }else{
            let newTask = new userModel({
                name: req.body.name
            })
            newTask.save().then((result, err) => {
                
                if(result){
                    return res.send(`New user registered`)
                }else{
                    return res.status(500).send(err)
                }
            })
        }
    }) 
    
})

app.listen(PORT, () => console.log(`Connected to PORT ${PORT}`))
